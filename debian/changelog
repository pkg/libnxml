libnxml (0.18.3-8apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 23:06:14 +0000

libnxml (0.18.3-8) unstable; urgency=medium

  * d/control:
    * the home page is now Github
    * bump standards to 4.4.1
  * Autopkgtests:
    * Fix gcc arguments ordering to avoid compilation failures
    * Removing tests using easy.c as it calls out a dead website

 -- Joseph Herlant <aerostitch@debian.org>  Sun, 20 Oct 2019 14:50:22 -0700

libnxml (0.18.3-7co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 01:21:59 +0000

libnxml (0.18.3-7) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/changelog: Remove trailing whitespaces

  [ Joseph Herlant ]
  * d/control:
    * Set Vcs-* to salsa.debian.org.
    * upgrade standards to 4.3.0.
    * Use the new debhelper-compat(=12) notation and drop d/compat.
    * Update my email to my new debian.org one.
  * d/watch: repo has moved to github.
  * Remove unnecessary dependency on autoreconf.
  * Switch to automatic -dbgsym.
  * d/rules:
    * should be executable.
    * d/rules: set hardening to all.
  * Add Build-Depends-Package to the symbols file.
  * Add an upstream/metadata file.
  * Run the examples in autopkgtests.
  * Add patch to avoid unnecessary failures in autopkgtest.

 -- Joseph Herlant <aerostitch@debian.org>  Sun, 20 Jan 2019 16:00:14 -0800

libnxml (0.18.3-6) unstable; urgency=medium

  * Updating description for multiarchify_pc_file.patch
  * Removing Makefile from the examples (Closes: #742314)

 -- Joseph Herlant <herlantj@gmail.com>  Sun, 23 Mar 2014 14:20:50 +0100

libnxml (0.18.3-5) unstable; urgency=medium

  * New maintainer. (Closes: #638041)
  * debian/control:
    (Maintainer) New maintainer
    (Standards-Version) Switching to 3.9.5
    (Build-Depends) upgrading to debhelper 9 and dh-autoreconf
    (Vcs-*) Moving repos to collab-maint
    Package short description update to avoid duplicate-short-description
  * debian/compat: moving to 9
  * debian/rules: moving to dh auto, debhelper v9, dh_strip
  * debian/libnxml0-dev.install: moving to lib/*/ scheme
  * debian/libnxml0.install: moving to lib/*/ scheme
  * debian/libnxml0.symbols: Adding symbol file
  * debian/copyright: updating to format 1.0

 -- Joseph Herlant <herlantj@gmail.com>  Sat, 15 Mar 2014 17:40:49 +0100

libnxml (0.18.3-4) unstable; urgency=low

  * QA upload.
  * Bump Standards-Version to 3.9.2.
  * Convert to Source format 3.0 (quilt), and drop quilt build-dependency and
    patchsys-quilt.
  * Stop shipping .la file altogether, and stop patching it.
  * Fix debian/copyright to use the © sign instead of (C).
  * Let libnxml0-dev depend on a non-virtual package before the virtual one.

 -- Regis Boudin <regis@debian.org>  Tue, 23 Aug 2011 23:43:46 +0100

libnxml (0.18.3-3) unstable; urgency=low

  * QA upload.
  * Orphaning package.

 -- Torsten Werner <twerner@debian.org>  Tue, 16 Aug 2011 21:23:36 +0200

libnxml (0.18.3-2.2) unstable; urgency=low

  * Non-maintainer upload.
  * debian/control:
    - changed debug's binary package to its
      proper archive section. Hopefully dak will
      be happy again.

 -- Andrea Veri <and@debian.org>  Tue, 16 Aug 2011 17:40:10 +0200

libnxml (0.18.3-2.1) unstable; urgency=medium

  [ Vincent Legout ]
  * Non-maintainer upload.
  * debian/rules: Empty dependency_libs in libnxml.la (Closes: #633315)

  [ Andrea Veri ]
  * Urgency set to medium since it fixes an RC bug.
  * debian/control:
    - libnxml was actually depending on two virtual packages:
      libcurl4-dev and libcurl-dev. Both of them are now provided
      by the package libcurl4-gnutls-dev. Updating B-Ds accordingly.

 -- Andrea Veri <and@debian.org>  Tue, 09 Aug 2011 21:01:42 +0200

libnxml (0.18.3-2) unstable; urgency=low

  [ Torsten Werner ]
  * Remove dummy symlink to /bin/true.

  [ Varun Hiremath ]
  * Add nxml_parser.diff patch to not strip leading spaces out of feeds
    (Closes: #496765)

 -- Varun Hiremath <varun@debian.org>  Tue, 09 Sep 2008 22:54:52 -0400

libnxml (0.18.3-1) unstable; urgency=low

  * new upstream bugfix release
  * Bump up Standards-Version: 3.8.0 (no changes needed).

 -- Torsten Werner <twerner@debian.org>  Sun, 24 Aug 2008 15:22:28 +0200

libnxml (0.18.2-3) unstable; urgency=low

  * Remove libnxml-depends after understanding that dpkg-shlibdeps has all the
    needed functionality.
  * Keep a dummy symlink to /bin/true to avoid breaking packages that call
    libnxml-depends.

 -- Torsten Werner <twerner@debian.org>  Sat, 05 Apr 2008 18:23:08 +0200

libnxml (0.18.2-2) unstable; urgency=low

  * Upload to unstable.

 -- Torsten Werner <twerner@debian.org>  Sun, 09 Mar 2008 23:08:27 +0100

libnxml (0.18.2-1) experimental; urgency=low

  * new upstream release
  * Upload to experimental to not disturb reverse depends yet.

 -- Torsten Werner <twerner@debian.org>  Sun, 02 Mar 2008 12:31:34 +0100

libnxml (0.18.1-4) unstable; urgency=low

  [ Varun Hiremath ]
  * Add libnxml-depends script to generate strict dependencies for the
    libnxml library

  [ Torsten Werner ]
  * Add Provides: libnxml-abi-${source:Upstream-Version} to package libnxml0.
  * Install libnxml-depends in /usr/bin/.
  * Add Varun to the AUTHORS section in libnxml-depends.in.

 -- Torsten Werner <twerner@debian.org>  Sat, 23 Feb 2008 18:39:24 +0100

libnxml (0.18.1-3) unstable; urgency=low

  * Bump Standards-Version to 3.7.3
  * Bump debhelper compat to 6
  * Add libnxml0 to libnxml0-dbg Depends (Closes: #463183)

 -- Varun Hiremath <varun@debian.org>  Wed, 30 Jan 2008 09:25:06 +0530

libnxml (0.18.1-2) unstable; urgency=low

  * Include test/* files as examples in libnxml0-dev package.

 -- Varun Hiremath <varunhiremath@gmail.com>  Mon, 22 Oct 2007 23:42:27 +0530

libnxml (0.18.1-1) unstable; urgency=low

  * New upstream release.

 -- Varun Hiremath <varunhiremath@gmail.com>  Sat, 22 Sep 2007 11:04:47 +0530

libnxml (0.18.0-1) unstable; urgency=low

  * New upstream release.

 -- Varun Hiremath <varunhiremath@gmail.com>  Fri, 03 Aug 2007 08:12:31 +0530

libnxml (0.17.3-2) unstable; urgency=low

  * Add a versioned shlib dependency that always requires the newest upstream
    version because we cannot count on upstream's author to update the library
    version correctly.

 -- Torsten Werner <twerner@debian.org>  Sun,  1 Jul 2007 23:28:53 +0200

libnxml (0.17.3-1) unstable; urgency=low

  * New upstream release

 -- Varun Hiremath <varunhiremath@gmail.com>  Fri, 08 Jun 2007 21:01:58 +0530

libnxml (0.17.2-5) unstable; urgency=low

  * Changed (Build-)Depends: libcurl4-dev | libcurl-dev. (Closes: #424040)

 -- Torsten Werner <twerner@debian.org>  Tue, 15 May 2007 20:28:05 +0200

libnxml (0.17.2-4) unstable; urgency=low

  * Fully switch to libcurl4-dev and remove any flexibility because there are
    strange bugs in the build process. (Closes: #423536)

 -- Torsten Werner <twerner@debian.org>  Sun, 13 May 2007 00:35:41 +0200

libnxml (0.17.2-3) unstable; urgency=low

  * Every buildd creates broken packages but pbuilder does not; add -v to
    curl-depends to get an idea.

 -- Torsten Werner <twerner@debian.org>  Sat, 12 May 2007 19:24:45 +0200

libnxml (0.17.2-2) unstable; urgency=low

  * Start transition to libcurl4-dev.
  * Add debian/curl-depends to automatically calculate Depends.

 -- Torsten Werner <twerner@debian.org>  Sat, 12 May 2007 11:43:59 +0200

libnxml (0.17.2-1) unstable; urgency=low

  * New upstream release: fixes a bug about empty attributes.

 -- Torsten Werner <twerner@debian.org>  Thu, 19 Apr 2007 00:13:37 +0200

libnxml (0.17.1-1) unstable; urgency=low

  * New upstream release

 -- Varun Hiremath <varunhiremath@gmail.com>  Fri, 23 Mar 2007 15:43:33 +0530

libnxml (0.17-1) unstable; urgency=low

  * New upstream release

 -- Varun Hiremath <varunhiremath@gmail.com>  Fri, 16 Mar 2007 17:35:51 +0530

libnxml (0.16-2) unstable; urgency=medium

  * Add libcurl3-dev to libnxml0 Depends (Closes: #407113)
  * Set urgency to medium because bug is RC.

 -- Varun Hiremath <varunhiremath@gmail.com>  Tue, 16 Jan 2007 17:22:56 +0530

libnxml (0.16-1) unstable; urgency=low

  * New upstream release.
  * Remove patches/pkg-config.diff, merged upstream.
  * Remove patches/clean_debian.diff, corrected upstream.

 -- Varun Hiremath <varunhiremath@gmail.com>  Fri, 29 Dec 2006 22:20:03 +0530

libnxml (0.15-1) unstable; urgency=low

  [ Varun Hiremath ]
  * Initial Release. (Closes: #404211)

  [ Torsten Werner ]
  * Add myself to Uploaders in debian/control.
  * Simplify the build process a little bit.
  * Install nxml.pc into /usr/lib/pkgconfig/.
  * Add a patch for nxml.pc.in.
  * Add a package libnxml0-dbg with debugging symbols.
  * Install the libtool file, too.

 -- Torsten Werner <twerner@debian.org>  Thu, 28 Dec 2006 23:55:56 +0100
